<?php
/**
 * @file
 * @todo Add file header description
 */

/**
 * @defgroup tripal_network_api Network Module API
 * @ingroup tripal_api
 * @ingroup tripal_network
 */


/**
 * Retrieve properties of a given type for a given network
 *
 * @param $network_id
 *    The network_id of the properties you would like to retrieve
 * @param $property
 *    The cvterm name of the properties to retrieve
 *
 * @return
 *    An network chado variable with the specified properties expanded
 *
 * @ingroup tripal_network_api
 */
function tripal_network_get_property($network_id, $property) {
  return tripal_core_get_property('network', $network_id, $property, 'network_property');
}

/**
 * Insert a given property
 *
 * @param $network_id
 *   The network_id of the property to insert
 * @param $property
 *   The cvterm name of the property to insert
 * @param $value
 *   The value of the property to insert
 * @param $update_if_present
 *   A boolean indicated whether to update the record if it's already present
 *
 * @return
 *   True of success, False otherwise
 *
 * @ingroup tripal_network_api
 */
function tripal_network_insert_property($network_id, $property, $value, $update_if_present = 0) {
  return tripal_core_insert_property('network', $network_id, $property, 'network_property', $value, $update_if_present);
}

/**
 * Update a given property
 *
 * @param $network_id
 *   The network_id of the property to update
 * @param $property
 *   The cvterm name of the property to update
 * @param $value
 *   The value of the property to update
 * @param $insert_if_missing
 *   A boolean indicated whether to insert the record if it's absent
 *
 * Note: The property will be identified using the unique combination of the $network_id and $property
 * and then it will be updated with the supplied value
 *
 * @return
 *   True of success, False otherwise
 *
 * @ingroup tripal_network_api
 */
function tripal_network_update_property($network_id, $property, $value, $insert_if_missing = 0) {
  return tripal_core_update_property('network', $network_id, $property, 'network_property', $value, $insert_if_missing);
}
/**
 * Delete a given property
 *
 * @param $network_id
 *   The network_id of the property to delete
 * @param $property
 *   The cvterm name of the property to delete
 *
 * Note: The property will be identified using the unique combination of the $network_id and $property
 * and then it will be deleted
 *
 * @return
 *   True of success, False otherwise
 *
 * @ingroup tripal_network_api
 */
function tripal_network_delete_property($network_id, $property) {
  return tripal_core_delete_property('network', $network_id, $property, 'network_property');
}

/**
 * Retrieve topological properties of a module
 *
 * @param $networkmod_id
 *    The module ID (networkmod_id) of the properties you would like to retrieve
 * @param $property
 *    A list of properties to retrieve.  These properties are calculated on the fly when 
 *    the function call is made and are not stored in the database. The list of available 
 *    properties include:
 *      edge_count
 *      node_count
 *      averge_degree
 *
 * @return
 *    No value is returned, but the $property array passed as the second argument
 *    is updated with the requested values
 *
 * @ingroup tripal_network_api
 */
function tripal_network_module_topological_properties($networkmod_id, &$property) {
  $result = array();
  
  // if the user requests the average degree then we also need 
  // to get the node and edge counts too
  if (key_exists('average_degree', $property)) {
    $property['node_count'] = 0;
    $property['edge_count'] = 0;
  } 
  
  if (key_exists('node_count', $property)) {
    $values = array('networkmod_id' => $networkmod_id);
    $options = array('statement_name' => 'sel_networkmod_node_mo');
    $columns = array('count(*) as node_count');
    $result = tripal_core_chado_select('networkmod_node', $columns, $values, $options);
    if ($result) {
      $property['node_count'] = $result[0]->node_count;
    }
  }
  if (key_exists('edge_count', $property)) {
    $values = array('networkmod_id' => $networkmod_id);
    $options = array('statement_name' => 'sel_networkmod_edge_mo');
    $columns = array('count(*) as edge_count');
    $result = tripal_core_chado_select('networkmod_edge', $columns, $values, $options);
    if ($result) {
      $property['edge_count'] = $result[0]->edge_count;
    }
  }
  if (key_exists('average_degree', $property)) {
    $property['average_degree'] = 0;
    if ($property['node_count'] > 0) {
     $property['average_degree'] = 2 * ((float) $property['edge_count']  / (float) $property['node_count'] );               
    }
  }
}
/**
 * Retrieve topological properties of a network
 *
 * @param $network_id
 *    The network_id of the properties you would like to retrieve
 * @param $property
 *    A list of properties to retrieve.  These properties are calculated on the fly when 
 *    the function call is made and are not stored in the database. The list of available 
 *    properties include:
 *      module_count
 *      edge_count
 *      node_count
 *      averge_degree
 *
 * @return
 *    No value is returned, but the $property array passed as the second argument
 *    is updated with the requested values
 *
 * @ingroup tripal_network_api
 */
function tripal_network_network_topological_properties($network_id, &$property) {
  $result = array();
  
  // if the user requests the average degree then we also need 
  // to get the node and edge counts too
  if (key_exists('average_degree', $property)) {
    $property['node_count'] = 0;
    $property['edge_count'] = 0;
  } 
  if (key_exists('module_count', $property)) {
    $values = array('network_id' => $network_id);
    $options = array('statement_name' => 'sel_networkmod_id');
    $columns = array('count(*) as module_count');
    $result = tripal_core_chado_select('networkmod', $columns, $values, $options);
    if ($result) {
      $property['module_count'] = $result[0]->module_count;
    }
  }
  if (key_exists('node_count', $property)) {
    $values = array('network_id' => $network_id);
    $options = array('statement_name' => 'sel_networknode_id');
    $columns = array('count(*) as node_count');
    $result = tripal_core_chado_select('networknode', $columns, $values, $options);
    if ($result) {
      $property['node_count'] = $result[0]->node_count;
    }
  }
  if (key_exists('edge_count', $property)) {
    $values = array('network_id' => $network_id);
    $options = array('statement_name' => 'sel_networkedge_id');
    $columns = array('count(*) as edge_count');
    $result = tripal_core_chado_select('networkedge', $columns, $values, $options);
    if ($result) {
      $property['edge_count'] = $result[0]->edge_count;
    }
  }
  if (key_exists('average_degree', $property)) {
    $property['average_degree'] = 0;
    if ($property['node_count'] > 0) {
     $property['average_degree'] = 2 * ((float) $property['edge_count']  / (float) $property['node_count'] );               
    }
  }
}