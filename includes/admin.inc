<?php
/**
 * @defgroup tripal_network Network
 * @{
 * Provides functions for managing biological networks including creating details pages for each network
 * @}
 * @ingroup tripal_modules
 */

/**
 * Purpose: Provide Guidance to new Tripal Admin
 *
 * @return HTML Formatted text
 *
 * @ingroup tripal_network
 */
function tripal_network_module_description_page() {
  $text = '';
  $text .= '<h3>Tripal Network Administrative Tools Quick Links:</h3>';
  $text .= "<ul>
             <li><a href=\"" . url("admin/tripal/tripal_network/configuration") . "\">Network Configuration</a></li>
           </ul>";
  $text .= '<h3>Module Description:</h3>';
  $text .= '<p>The Tripal Network module is an interface for the Chado Network module which groups features (sequences) into genetic networks.
    This module provides support for visualization of "network" pages, editing and updating.</p>';

  $text .= '<h3>Setup Instructions:</h3>';
  $text .= '<ol>';
  $text .= '<li><p><b>Set Permissions</b>: The network module supports the Drupal user permissions interface for
               controlling access to network content and functions. These permissions include viewing,
               creating, editing or administering of
               network content. The default is that only the original site administrator has these
               permissions.  You can <a href="' . url('admin/user/roles') . '">add roles</a> for classifying users,
               <a href="' . url('admin/user/user') . '">assign users to roles</a> and
               <a href="' . url('admin/user/permissions') . '">assign permissions</a> for the network content to
               those roles.  For a simple setup, allow anonymous users access to view organism content and
               allow the site administrator all other permissions.</p></li>';
  $text .= '<li><p><b>Sync any Existing networks</b>: Near the top of the ' . l('Network Configuration page', 'admin/tripal/tripal_network/configuration') . ' there is
              a Sync networks section which provides list of networks currently in chado which can be sync\'d.
              Simply select the networks you would like to create Drupal/Tripal pages for and click Sync networks.</p></li>';
  $text .= '</ol>';


  $text .= '<h3>Features of this Module:</h3>';
  $text .= '<ul>';
    $text .= '<li><b>Add/Edit/Delete networks</b>: networks with no associated features can be created ' . l('here', 'node/add/chado-network') . ' but it is
              recommended to create the network using the feature loader. For example, when you load FASTA files using the Tripal loader you are
              given the option of specifying a network for all created features. Existing networks (regardless of the method used to create them) can be
              edited or deleted by clicking the Edit tab at the top of the Network Page.</li>';
    $text .= '<li><p><b>Integration with Drupal Views</b>: <a href="http://drupal.org/project/views">Drupal Views</a> is
              a powerful tool that allows the site administrator to create lists or basic searching forms of Chado content.
              It provides a graphical interface within Drupal to allow the site admin to directly query the Chado database
              and create custom lists without PHP programming or customization of Tripal source code.  Views can also
              be created to filter content that has not yet been synced with Druapl in order to protect access to non
              published data (only works if Chado was installed using Tripal).  You can see a list of available pre-existing
              Views <a href="' . url('admin/build/views/') . '">here</a>, as well as create your own. </p></li>';
    $text .= '<li><b>Basic Listing</b>: This module provides a basic <a href="' . url('networks') . '">network display
              tool</a> for finding or listing networks in Chado. It does not require indexing for Drupal searching but relies
              on Drupal Views.  <a href="http://drupal.org/project/views">Drupal Views</a> must be installed.</li>';
  $text .= '</ul>';

  $text .= '<h3>Page Customizations</h3>';
  $text .= '<p>There are several ways to customize the look-and-feel for the way Chado data is presented through Tripal.
             Below is a description of several methods.  These methods may be used in conjunction with one another to
             provide fine-grained control.
             <ul>

             <li><p><b>Integration with Drupal Panels</b>:  <a href="http://drupal.org/project/views">Drupal Panels</a>
              allows for customization of a page layout if you don\'t want to do PHP/Javascript/CSS programming.  Tripal comes with pre-set layouts for network pages.  However,
              Panels become useful if you prefer a layout that is different from the pre-set layouts.  Chado content
              is provided to Panels in the form of Drupal "blocks" which you can then place anywhere on a page using the
              Panel\'s GUI.</p></li>

             <li><p><b>Drupal\'s Content Construction Kit (CCK)</b>: the
             <a href="http://drupal.org/project/cck">Content Construction Kit (CCK) </a> is a powerful way to add non-Chado content
             to any page without need to edit template files or knowing PHP.  You must first download and install CCK.
             With CCK, the site administartor can create a new field to appear on the page.  For example, currently,
             the Chado publication module is not yet supported by Tripal.  Therefore, the site administrator can add a text
             field to the network pages.  This content is not stored in Chado, but will appear on the network page.  A field
             added by CCK will also appear in the form when editing a network to allow users to manually enter the appropriate
             text.  If the default pre-set layout and themeing for Tripal is used, it is better to create the CCK element,
             indicate that it is not to be shown (using the CCK interface), then manually add the new content type
             where desired by editing the templates (as described below).  If using Panels, the CCK field can be added to the
             location desired using the Panels interface.</p></li>

             <li><p><b>Drupal Node Templates</b>:  The Tripal packages comes with a "theme_tripal" directory that contains the
             themeing for Chado content.    The network module has a template file for network "nodes" (Tripal network pages).  This file
             is named "node-chado_network.tpl.php", and provides javascript, HTML and PHP code for display of the network
             pages.  You can edit this file to control which types of information (or which network "blocks") are displayed for networks. Be sure to
             copy these template to your primary theme directory for editing. Do not edit them in the "theme_tripal" directory as
             future Tripal updates may overwrite your customizations. See the <a href="http://tripal.sourceforge.net/">Tripal website </a>
             for instructions on how to access variables and other Chado content within the template file.</p></li>

             <li><p><b>Network "Block" Templates</b>:  In the "theme_tripal" directory is a subdirectory named "tripal_network".
             Inside this directory is a set of templates that control distinct types of information for networks.  For example,
             there is a "base" template for displaying of data directly from the Chado network table.  These templates are used both by Drupal blocks
             for use in Drupal Panels (as described above) or for use in the default pre-set layout that the node template
             provides (also desribed above).  You can customize this template as you desire.  Be sure to copy the
             template to your primary theme directory for editing. Do not edit them in the "theme_tripal" directory as
             future Tripal updates may overwrite your customizations.  See the <a href="http://tripal.sourceforge.net/">Tripal website </a>
             for instructions on how to access variables and other Chado content within the template files.</p></li>
             </li>

             <li><p><b>Adding Links to the "Resources" Sidebar</b>: If you use the pre-set default Tripal layout for theming, you
             will see a "Resources" sidebar on each page.  The links that appear on the sidebar are automatically generated
             using Javascript for all of the network "Blocks" that appear on the page. If you want to add additional links
             (e.g. a link to a views table showing all features of the current network) and you want that link to appear in the
             "Resources" sidebar, simply edit the Drupal Node Template (as described above) and add the link to the
             section at the bottom of the template file where the resources section is found.</p></li>

             </ul>
             </p>';

  return $text;
}

/**
 * Administrative settings form
 *
 * @ingroup tripal_network
 */
function tripal_network_admin() {
  $form = array();

  // before proceeding check to see if we have any
  // currently processing jobs. If so, we don't want
  // to give the opportunity to sync libraries
  $active_jobs = FALSE;
  if (tripal_get_module_active_jobs('tripal_network')) {
    $active_jobs = TRUE;
  }

  // add the field set for syncing libraries
  if (!$active_jobs) {
    get_tripal_network_admin_form_sync_set($form);
    get_tripal_network_admin_form_cleanup_set($form);
    get_tripal_network_admin_form_reindex_set($form);
    get_tripal_network_admin_form_taxonomy_set($form);
    get_tripal_network_admin_form_cleanup_set($form);
  }
  else {
    $form['notice'] = array(
     '#type' => 'fieldset',
     '#title' => t('Network Management Temporarily Unavailable')
    );
    $form['notice']['message'] = array(
        '#value' => t('Currently, Network management jobs are waiting or are running. . Managemment features have been hidden until these jobs complete.  Please check back later once these jobs have finished.  You can view the status of pending jobs in the Tripal jobs page.'),
    );
  }

  return system_settings_form($form);
}
/**
 *
 *
 * @ingroup tripal_network
 */
function get_tripal_network_admin_form_display_settings(&$form) {
  $form['display'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display Settings')
  );
  $form['display']['synonym'] = array(
     '#type' => 'item',
     '#value' => t(""),
  );
}
/**
 *
 *
 * @ingroup tripal_network
 */
function get_tripal_network_admin_form_cleanup_set(&$form) {
  $form['cleanup'] = array(
    '#type' => 'fieldset',
    '#title' => t('Clean Up')
  );
  $form['cleanup']['description'] = array(
     '#type' => 'item',
     '#value' => t("With Drupal and chado residing in different databases ".
        "it is possible that nodes in Drupal and networks in Chado become ".
        "\"orphaned\".  This can occur if an network node in Drupal is ".
        "deleted but the corresponding chado network is not and/or vice ".
        "versa. Click the button below to resolve these discrepancies."),
     '#weight' => 1,
  );
  $form['cleanup']['button'] = array(
    '#type' => 'submit',
    '#value' => t('Clean up orphaned networks'),
    '#weight' => 2,
  );
}

/**
 *
 *
 * @ingroup tripal_network
 */
function get_tripal_network_admin_form_taxonomy_set(&$form) {
  $form['taxonify'] = array(
    '#type' => 'fieldset',
    '#title' => t('Assign Drupal Taxonomy to Network Features')
  );

  // get the list of networks
  $sql = "SELECT * FROM {Network} ORDER BY uniquename";
  $previous_db = tripal_db_set_active('chado');  // use chado database
  $lib_rset = db_query($sql);
  tripal_db_set_active($previous_db);  // now use drupal database

  // iterate through all of the networks
  $net_boxes = array();
  while ($network = db_fetch_object($lib_rset)) {
    $net_boxes[$network->network_id] = "$network->name";
  }

  $form['taxonify']['description'] = array(
     '#type' => 'item',
     '#value' => t("Drupal allows for assignment of \"taxonomy\" or catagorical terms to " .
                   "nodes. These terms allow for advanced filtering during searching. This option allows ".
                   "for setting taxonomy only for features that belong to the selected networks below.  All other features will be unaffected.  To set taxonomy for all features in the site see the Feature Administration page."),
   '#weight' => 1,
  );

  $form['taxonify']['tx-networks'] = array(
   '#title'       => t('networks'),
   '#type'        => t('checkboxes'),
   '#description' => t("Check the networks whose features you want to reset taxonomy.  Note: this list contains all networks, even those that may not be synced."),
   '#required'    => FALSE,
   '#prefix'      => '<div id="net_boxes">',
   '#suffix'      => '</div>',
   '#options'     => $net_boxes,
   '#weight'      => 2
  );
  $form['taxonify']['tx-button'] = array(
    '#type' => 'submit',
    '#value' => t('Set Feature Taxonomy'),
    '#weight'      => 3
  );
}
/**
 *
 * @ingroup tripal_network
 */
function get_tripal_network_admin_form_reindex_set(&$form) {
   // define the fieldsets
  $form['reindex'] = array(
    '#type' => 'fieldset',
    '#title' => t('Reindex Network Features')
  );

  // get the list of networks
  $sql = "SELECT * FROM {Network} ORDER BY uniquename";
  $previous_db = tripal_db_set_active('chado');  // use chado database
  $lib_rset = db_query($sql);
  tripal_db_set_active($previous_db);  // now use drupal database

  // iterate through all of the networks
  $net_boxes = array();
  while ($network = db_fetch_object($lib_rset)) {
    $net_boxes[$network->network_id] = "$network->name";
  }
  $form['reindex']['description'] = array(
     '#type' => 'item',
     '#value' => t("This option allows for reindexing of only those features that belong to the selected networks below. All other features will be unaffected.  To reindex all features in the site see the Feature Administration page."),
   '#weight' => 1,
  );

  $form['reindex']['re-networks'] = array(
   '#title'       => t('networks'),
   '#type'        => t('checkboxes'),
   '#description' => t("Check the networks whoee features you want to reindex. Note: this list contains all networks, even those that may not be synced."),
   '#required'    => FALSE,
   '#prefix'      => '<div id="net_boxes">',
   '#suffix'      => '</div>',
   '#options'     => $net_boxes,
   '#weight' => 2,
  );
  $form['reindex']['re-button'] = array(
    '#type' => 'submit',
    '#value' => t('Reindex Features'),
    '#weight' => 3,
  );
}
/**
 *
 * @ingroup tripal_network
 */
function get_tripal_network_admin_form_sync_set(&$form) {
   // define the fieldsets
  $form['sync'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sync networks')
  );


  // get the list of networks
  $sql = "SELECT * FROM network ORDER BY uniquename";
  $previous_db = tripal_db_set_active('chado');  // use chado database
  $lib_rset = db_query($sql);
  tripal_db_set_active($previous_db);  // now use drupal database

  // if we've added any networks to the list that can be synced
  // then we want to build the form components to allow the user
  // to select one or all of them.  Otherwise, just present
  // a message stating that all networks are currently synced.
  $net_boxes = array();
  $added = 0;
  while ($network = db_fetch_object($lib_rset)) {
    // check to see if the network is already present as a node in drupal.
    // if so, then skip it.
    $sql = "SELECT * FROM {chado_network} WHERE network_id = %d";
    if (!db_fetch_object(db_query($sql, $network->network_id))) {
      $net_boxes[$network->network_id] = "$network->uniquename";
      $added++;
    }
  }

  // if we have networks we need to add to the checkbox then
  // build that form element
  if ($added > 0) {
    $net_boxes['all'] = "All networks";

    $form['reindex']['description'] = array(
     '#type' => 'item',
     '#value' => t("This option allows for the creation of Drupal content for networks in chado. Only the selected networks will be synced."),
     '#weight' => 1,
    );


    $form['sync']['networks'] = array(
      '#title'       => t('Available networks'),
      '#type'        => t('checkboxes'),
      '#description' => t("Check the networks you want to sync.  Drupal content will be created for each of the networks listed above.  Select 'All networks' to sync all of them."),
      '#required'    => FALSE,
      '#prefix'      => '<div id="net_boxes">',
      '#suffix'      => '</div>',
      '#options'     => $net_boxes,
    '#weight' => 2,
    );
    $form['sync']['button'] = array(
       '#type' => 'submit',
       '#value' => t('Sync networks'),
     '#weight' => 3,
    );
  }
   // we don't have any networks to select from
  else {
    $form['sync']['value'] = array(
       '#value' => t('All networks in Chado are currently synced with Drupal.')
    );
  }
}
/**
 *
 * @ingroup tripal_network
 */
function tripal_network_admin_validate($form, &$form_state) {
  global $user;  // we need access to the user info
  $job_args = array();

  // Submit the Sync Job if selected
  if ($form_state['values']['op'] == t('Sync networks')) {

    // check to see if the user wants to sync chado and drupal.  If
    // so then we need to register a job to do so with tripal
    $networks = $form_state['values']['networks'];
    $do_all = FALSE;
    $to_sync = array();

  foreach ($networks as $network_id) {
    if (preg_match("/^all$/i", $network_id)) {
      $do_all = TRUE;
    }
    if ($network_id and preg_match("/^\d+$/i", $network_id)) {
      // get the network info
      $sql = "SELECT * FROM {Network} WHERE network_id = %d";
      $previous_db = tripal_db_set_active('chado');  // use chado database
      $network = db_fetch_object(db_query($sql, $network_id));
      tripal_db_set_active($previous_db);  // now use drupal database
      $to_sync[$network_id] = $network->name;
    }
  }

  // submit the job to the tripal job manager
  if ($do_all) {
    tripal_add_job('Sync all networks', 'tripal_network', 'tripal_network_sync_networks', $job_args, $user->uid);
  }
  else{
    foreach ($to_sync as $network_id => $name) {
      $job_args[0] = $network_id;
      tripal_add_job("Sync network: $name", 'tripal_network', 'tripal_network_sync_networks', $job_args, $user->uid);
      }
    }
  }

  // -------------------------------------
  // Submit the Reindex Job if selected
  if ($form_state['values']['op'] == t('Reindex Features')) {
    $networks = $form_state['values']['re-networks'];
    foreach ($networks as $network_id) {
      if ($network_id and preg_match("/^\d+$/i", $network_id)) {
        // get the network info
        $sql = "SELECT * FROM {Network} WHERE network_id = %d";
        $previous_db = tripal_db_set_active('chado');  // use chado database
        $network = db_fetch_object(db_query($sql, $network_id));
        tripal_db_set_active($previous_db);  // now use drupal database
        $job_args[0] = $network_id;
        tripal_add_job("Reindex features for network: $network->name", 'tripal_network',
         'tripal_network_reindex_features', $job_args, $user->uid);
      }
    }
  }

  // -------------------------------------
  // Submit the Taxonomy Job if selected
  if ($form_state['values']['op'] == t('Set Feature Taxonomy')) {
    $networks = $form_state['values']['tx-networks'];
    foreach ($networks as $network_id) {
      if ($network_id and preg_match("/^\d+$/i", $network_id)) {
        // get the network info
        $sql = "SELECT * FROM {Network} WHERE network_id = %d";
        $previous_db = tripal_db_set_active('chado');  // use chado database
        $network = db_fetch_object(db_query($sql, $network_id));
        tripal_db_set_active($previous_db);  // now use drupal database
        $job_args[0] = $network_id;
        tripal_add_job("Set taxonomy for features in network: $network->name", 'tripal_network',
         'tripal_network_taxonify_features', $job_args, $user->uid);
      }
    }
  }
    // -------------------------------------
    // Submit the Cleanup Job if selected
    if ($form_state['values']['op'] == t('Clean up orphaned networks')) {
      tripal_add_job('Cleanup orphaned networks', 'tripal_network',
         'tripal_network_cleanup', $job_args, $user->uid);
    }
}


/**
 * Add the network as a taxonomy term for associating with network_features
 *
 * @ingroup tripal_network
 */
function tripal_network_add_taxonomy($node, $network_id) {

    //include the file containing the required functions.  We only have to
    // do this because Drupal 6 fails to do this globally for us and
    // the drupal_execute function below won't work
    module_load_include('inc', 'taxonomy', 'taxonomy.admin');

   /*   // get the vocabulary id
    $vocabularies = taxonomy_get_vocabularies();
    $vid = NULL;
    foreach($vocabularies as $vocab){
    if($vocab->name == 'DNA networks'){
    $vid = $vocab->vid;
    }
    }

    if(!$vid){  */
   // add the vocabulary
  $vocab_form['values']['name'] = 'DNA networks';
  $vocab_form['values']['description'] = 'Allows for associating/searching of network features by network name';
  $vocab_form['values']['help'] = '';
  $vocab_form['values']['module'] = 'taxonomy';
  drupal_execute('taxonomy_form_vocabulary', $vocab_form);
  return;
   //   }

   // make sure this term doesn't already exist.  If it doesn't then add it
  if ($vid) {
    $tree = taxonomy_get_tree($vid);
    $found = 0;
    foreach ($tree as $term) {
      if ($term->name == $node->title) {
        $found = 1;
      }
    }

      // add the term to the vocabulary
    if (!$found) {
      $form_state = array();
      $form_state['values']['name'] = $node->title;
      $form_state['values']['description'] = $network_id;
      drupal_execute('taxonomy_form_term', $form_state, $vid);
    }
  }
}

/**
 *
 *
 * @ingroup tripal_network
 */
function tripal_network_sync_networks($network_id = NULL, $job_id = NULL) {

  global $user;
  $page_content = '';

  // get the list of networks and create new nodes
  if (!$network_id) {
    $sql = "SELECT * FROM network N";
    $previous_db = tripal_db_set_active('chado');  // use chado database
    $results = db_query($sql);
    tripal_db_set_active($previous_db);  // now use drupal database
  }
  else {
    $sql = "SELECT * FROM network n WHERE network_id = %d";
    $previous_db = tripal_db_set_active('chado');  // use chado database
    $results = db_query($sql, $network_id);
    tripal_db_set_active($previous_db);  // now use drupal database
  }

  // We'll use the following SQL statement for checking if the network
  // already exists as a drupal node.
  $sql = "SELECT * FROM {chado_network} ".
        "WHERE network_id = %d";

  while ($network = db_fetch_object($results)) {

    // check if this network already exists in the drupal database. if it
    // does then skip this network and go to the next one.
    if (!db_fetch_object(db_query($sql, $network->network_id))) {

      $new_node = new stdClass();
      $new_node->type = 'chado_network';
      $new_node->uid = $user->uid;
      $new_node->title = "$network->name";
      $new_node->network_id = $network->network_id;
      $new_node->organism_id = $network->organism_id;
      $new_node->uniquename = $network->uniquename;
      $new_node->type_id = $network->type_id;
  
      node_validate($new_node);
      $errors = form_get_errors();
      if (!$errors) {
        $node = node_submit($new_node);
        node_save($node);
        if ($node->nid) {
          print "Added " . $network->uniquename . "\n";
        }
        else {
          print "ERROR: Unable to create " . $network->name . "\n";
        }
      }
      else {
        print "ERROR: Unable to create " . $network->name . "\n" . print_r($errors, TRUE) . "\n";
      }
    }
    else {
      print "Skipped " . $network->name . "\n";
    }
  }
  return $page_content;
}


/**
 *
 * @ingroup tripal_network
 */
function tripal_network_feature_set_taxonomy($network_id = NULL) {

   //TO DO : return usable error if vocabs don't exist
   // get the list of vocabularies and find our two vocabularies of interest
  $vocabularies = taxonomy_get_vocabularies();
  $vid = NULL;
  foreach ($vocabularies as $vocab) {
    if ($vocab->name == 'Network') {
      $vid = $vocab->vid;
    }
  }
  if (!$vid) {
    return;
  }

  // We'll use the following SQL statement for getting the node info
  if ($network_id) {
    print "Finding features for network with ID: $network_id\n";
    $sql = "SELECT LF.feature_id, L.network_id, L.name as libname ".
           "FROM {network_feature} LF ".
           "INNER JOIN Network L ON LF.network_id = L.network_id ".
           "WHERE L.network_id = $network_id ".
           "ORDER BY LF.feature_id";
    $previous_db = tripal_db_set_active('chado');  // use chado database
    $features = db_query($sql);
    tripal_db_set_active($previous_db);  // now use drupal database
  }
  else {
    print "Finding features for all networks\n";
    $sql = "SELECT LF.feature_id, L.network_id, L.name as libname ".
           "FROM {network_feature} LF ".
           "INNER JOIN Network L ON LF.network_id = L.network_id ".
           "ORDER BY LF.feature_id";
    $previous_db = tripal_db_set_active('chado');  // use chado database
    $features = db_query($sql);
    tripal_db_set_active($previous_db);  // now use drupal database
  }

  $node_sql = "SELECT * FROM {chado_feature} CF ".
             "  INNER JOIN {node} N ON CF.nid = N.nid ".
             "WHERE feature_id = %d";

  // iterate through the features and add the taxonomy
  while ($feature = db_fetch_object($features)) {
    $node = db_fetch_object(db_query($node_sql, $feature->feature_id));
    $tags["$vid"] = $feature->libname;
    $terms['tags'] = $tags;
    taxonomy_node_save($node, $terms);
    print "Updated $feature->feature_id as $feature->libname\n";
  }
}
/**
 *
 * @ingroup tripal_network
 */
function tripal_network_reindex_features($network_id = NULL, $job_id = NULL) {
  $i = 0;

  // if the caller provided a network_id then get all of the features
  // associated with the network. Otherwise get all sequences assoicated
  // with all networks.
  if ($network_id) {
    $sql = "SELECT LF.feature_id, L.network_id, L.name as libname ".
           " FROM {network_feature} LF ".
           "  INNER JOIN Network L ON LF.network_id = L.network_id ".
           "WHERE L.network_id = $network_id ".
           "ORDER BY LF.feature_id";
    $previous_db = tripal_db_set_active('chado');  // use chado database
    $results = db_query($sql);
    tripal_db_set_active($previous_db);  // now use drupal database
  }
  else {
    $sql = "SELECT LF.feature_id, L.network_id, L.name as libname ".
           " FROM {network_feature} LF ".
           "  INNER JOIN Network L ON LF.network_id = L.network_id ".
           "ORDER BY LF.feature_id";
    $previous_db = tripal_db_set_active('chado');  // use chado database
    $results = db_query($sql);
    tripal_db_set_active($previous_db);  // now use drupal database
  }

   // load into ids array
  $count = 0;
  $ids = array();
  while ($id = db_fetch_object($results)) {
    $ids[$count] = $id->feature_id;
    $count++;
  }

  $interval = intval($count * 0.01);
  foreach ($ids as $feature_id) {
    // update the job status every 1% features
    if ($job_id and $i % interval == 0) {
      tripal_job_set_progress($job_id, intval(($i/$count)*100));
    }
    tripal_feature_sync_feature($feature_id);
    $i++;
  }
}
/**
 *
 * @ingroup tripal_network
 */
function tripal_network_taxonify_features($network_id = NULL, $job_id = NULL) {
  $i = 0;

  // if the caller provided a network_id then get all of the features
  // associated with the network. Otherwise get all sequences assoicated
  // with all networks.
  if ($network_id) {
    $sql = "SELECT LF.feature_id, L.network_id, L.name as libname ".
           " FROM {network_feature} LF ".
           "  INNER JOIN Network L ON LF.network_id = L.network_id ".
           "WHERE L.network_id = $network_id ".
           "ORDER BY LF.feature_id";
    $previous_db = tripal_db_set_active('chado');  // use chado database
    $results = db_query($sql);
    tripal_db_set_active($previous_db);  // now use drupal database
  }
  else {
    $sql = "SELECT LF.feature_id, L.network_id, L.name as libname ".
           " FROM {network_feature} LF ".
           "  INNER JOIN Network L ON LF.network_id = L.network_id ".
           "ORDER BY LF.feature_id";
    $previous_db = tripal_db_set_active('chado');  // use chado database
    $results = db_query($sql);
    tripal_db_set_active($previous_db);  // now use drupal database
  }

  // load into ids array
  $count = 0;
  $ids = array();
  while ($id = db_fetch_object($results)) {
    $ids[$count] = $id->feature_id;
    $count++;
  }

  // make sure our vocabularies are set before proceeding
  tripal_feature_set_vocabulary();

  // use this SQL for getting the nodes
  $nsql =  "SELECT * FROM {chado_feature} CF ".
          "  INNER JOIN {node} N ON N.nid = CF.nid ".
          "WHERE feature_id = %d";

   // iterate through the features and set the taxonomy
  $interval = intval($count * 0.01);
  foreach ($ids as $feature_id) {
    // update the job status every 1% features
    if ($job_id and $i % interval == 0) {
      tripal_job_set_progress($job_id, intval(($i/$count)*100));
    }
      $node = db_fetch_object(db_query($nsql, $feature_id));
      tripal_feature_set_taxonomy($node, $feature_id);
      $i++;
  }
}

/**
 * Remove orphaned drupal nodes
 *
 * @param $dummy
 *   Not Used -kept for backwards compatibility
 * @param $job_id
 *   The id of the tripal job executing this function
 *
 * @ingroup tripal_network
 */
function tripal_network_cleanup($dummy = NULL, $job_id = NULL) {

  return tripal_core_clean_orphaned_nodes('network', $job_id);
  
}


