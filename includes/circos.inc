<?php
/**
 * @defgroup tripal_network 
 * @{
 * Provides support for integration with circos
 * @}
 * @ingroup tripal_modules
 */


/**
 *', '
 * @ingroup tripal_network
 */
function tripal_network_generate_circos_image() {
  
  // get the module_id passed in
  $module_id = $_GET['module_id'];
  $t = $_GET['traits'];
  $m = $_GET['maps'];
  

  $traits = array();
  if (!is_array($t) and $t) {
    $traits[] = $t;
  }
  else {
    $traits = $t;
  }
  
  $maps = array();
  if (!is_array($m) and $m) {
    $maps[] = $m;
  }
  else {
    $maps = $m;
  }
  
  // construct the directory path where circos will run
  $exec_dir = file_directory_path() . "/tripal/tripal_network/circos"; 
  
  // for file creation we need the full path
  $full_exec_dir = realpath('./') . "/" .  $exec_dir . "/" . $module_id;
  
  // construct the final image_url
  $image_url = url("$exec_dir/$module_id/module.$module_id.png");
  
  
  // create the array that will be returned on success
  $arr = array(
    'status' => TRUE,
    "image" => "<img src=\"$image_url\" border=\"0\">"
  );
  
  // if don't need to show traits and the file already exists then don't recreate it
  if (count($traits) == 0 and 
      file_exists("$exec_dir/$module_id/module.$module_id.png")) {
    return drupal_json($arr);
  }
	  
	// first create the circos direcotry
	if(!is_dir($full_exec_dir) and !mkdir($full_exec_dir, 0775, TRUE)){
		 watchdog('tripal_network', 'tripal_network_generate_circos_image. Cannot create directory %dir', 
		   array('%dir' => $exec_dir), WATCHDOG_ERROR);
	   return drupal_json(array('status' => FALSE, 'image' => $full_exec_dir));
	}
	  	  	  	 
	// get the organism for this module
	$sql = "SELECT O.organism_id ".
	       "FROM organism O " .
	       "  INNER JOIN network N on O.organism_id = N.organism_id " .
	       "  INNER JOIN networkmod NM on NM.network_id = N.network_id " .
	       "WHERE NM.networkmod_id = %d ";
	$organism = db_fetch_object(chado_query($sql, $module_id));
	  
	if ($organism) {  
		// first write the karyotype file
	  $karyotype_term = variable_get('karyotepe_term','chromosome');
	  $karyotype_file = tripal_network_write_karyotype_file($karyotype_term, $organism->organism_id, 
	    $module_id, $full_exec_dir);
	  
	  // next write the edge file and conf files
	  $edge_file = tripal_network_write_edge_file($module_id, $full_exec_dir, $karyotype_term);
	  
	  // write the QTL trait file
	  $qtl_file = tripal_network_write_qtl_file ($module_id, $full_exec_dir, $traits, $maps);
	  
	  // next, write the circos config file
	  $tiles = array();
	  $tiles[] = $qtl_file;
	  $conf_file = tripal_network_write_circos_config($karyotype_file, $edge_file, $tiles,
	    $module_id, $full_exec_dir);
	  
	  // run circos    
	  $ret = array();
	  $circos_binary = variable_get('circos_binary','/usr/local/circos/bin/circos');
	  exec("cd $full_exec_dir; $circos_binary", $ret);   
	  
	} 
	else {
		watchdog('tripal_network', 'tripal_network_generate_circos_image. Cannot find the organism for this module', array(), WATCHDOG_ERROR);
	  return drupal_json(array('status' => FALSE, 'image' => '', 'message' => 'no organism'));	
	}
  return drupal_json($arr);
}
/**
 *', '
 * @ingroup tripal_network
 */
function tripal_network_write_karyotype_file($so_type, $organism_id, $module_id, $circos_exec_dir) {
  	
  if(!$organism_id) {
     watchdog('tripal_network', "Cannot create circos edge file due to missing organism ID", array(), WATCHDOG_ERROR);
     return FALSE;
  }
  
  if(!$module_id) {
     watchdog('tripal_network', "Cannot create circos edge file due to missing module ID", array(), WATCHDOG_ERROR);
     return FALSE;
  }
  
  if(!$so_type) {
     watchdog('tripal_network', "Cannot create circos edge file due to missing landmark type (so_type)", array(), WATCHDOG_ERROR);
     return FALSE;
  }
  
  $CIRCOS_COLORS =  unserialize(variable_get('circos_colors', serialize(array())));
  $CHR_COLORS = unserialize(variable_get('circos_chr_colors', serialize(array())));
	
	// first, get the list of features for the karyotype for this organism
	$values = array(
	  'type_id' => array (
	    'name' => $so_type,
	    'cv_id' => array (
	      'name' => 'sequence'
	    )	  
	  ),
	  'organism_id' => $organism_id 
	);
	$columns = array('feature_id', 'name', 'seqlen');
	$options = array('statement_name' => 'sel_feature_ty', 'order_by' => array('feature_id' => 'ASC'));
	$results = tripal_core_chado_select('feature', $columns, $values, $options);
		
	// open the karyotype file for writing
	$karyotype_file = "$circos_exec_dir/karyotype.$organism_id.txt";
	$kfh = fopen($karyotype_file, 'w'); 
	
	// next iterate through each feature and save it to the karyotype file
	$i = 0;
	foreach ($results as $feature) {
		 $id = $feature->name;
     $name = $feature->name;
     $end = $feature->seqlen;
     // select the color to use for each chromosome/scaffold (etc.)
     // by default we want to try to use the chromosome colors 
     // given in the circos /etc/colors.conf file.  But we can only
     // properly use those if the names match (e.g. chr1, chr2).
		 if (in_array(strtolower($name), $CHR_COLORS)) {
		   $color = strtolower($name);
		 }
		 // if we can't match the feature name then just cycle through the
		 // available colors
		 else {
		   $color = 	$CIRCOS_COLORS[$i++];
		 }
		 
		 // write out each line of the karyotype file
		 // http://circos.ca/tutorials/lessons/ideograms/karyotypes/
		 fwrite($kfh, "chr - $id $name 0 $end $color\n");	   
	}
	fclose($kfh);
	
	// return the karyotype file name
	return $karyotype_file;

}
/**
 * 
 * @ingroup tripal_network
 */
function tripal_network_write_qtl_file ($module_id, $circos_exec_dir, $traits, $maps) {
  if(!$module_id) {
     watchdog('tripal_network', "Cannot create circos experiment file due to missing module ID", array(), WATCHDOG_ERROR);
     return FALSE;
  } 
  $args = array();
  $args[] = $module_id;
  
  $conf_file = "$circos_exec_dir/qtl.$module_id.conf";
  $cfh = fopen($conf_file, 'w');
  
  // build the SQL
  $sql = "
    SELECT map_name, trait_name, landmark, fmin, fmax
    FROM networkmod_qtl
    WHERE networkmod_id = %d 
  "; 
  
  // filter results to only show the specified traits
  if (count($traits) > 0) {
    $sql .= " and
      trait_dbxref_id IN (
    ";
    foreach ($traits as $trait_dbxref_id) {
      $sql .= "%d, ";
      $args[] = $trait_dbxref_id;
    }
    $sql = substr($sql, 0, -2);
    $sql .= ") ";
  }
  
  // filter resutls to only show the specified maps
  if (count($maps) > 0) {
    $sql .= " and
      featuremap_id IN (
    ";
    foreach ($maps as $featuremap_id) {
      $sql .= "%d, ";
      $args[] = $featuremap_id;
    }
    $sql = substr($sql, 0, -2);
    $sql .= ") ";
  }
  
  // sort the results
  $sql .= "ORDER by map_name, trait_name";

  // perform the query
  $results = chado_query($sql, $args);
  
  // generate the plot file
  $current_map = '';
  $current_trait = '';
  $i = 0; 
  $CIRCOS_COLORS =  unserialize(variable_get('circos_colors', serialize(array())));
  while ($qtl = db_fetch_object($results)) {
    if ($current_map != $qtl->map_name or $current_trait != $qtl->trait_name) {
      if ($i + 1 >= count($CIRCOS_COLORS)) {
        $i = 0;
      }
      $color = $CIRCOS_COLORS[$i++];
      $current_map = $qtl->map_name;
      $current_trait = $qtl->trait_name;
    } 
    $landmark = $qtl->landmark;
    $fmin = $qtl->fmin;
    $fmax = $qtl->fmax;
    fwrite($cfh, "$landmark $fmin $fmax color=$color\n");
  }
  fclose($cfh); 

  return $conf_file;
}

/**
 * 
 * @ingroup tripal_network
 */
function tripal_network_write_edge_file ($module_id, $circos_exec_dir, $so_type) {
	
  if(!$module_id) {
     watchdog('tripal_network', "Cannot create circos edge file due to missing module ID", array(), WATCHDOG_ERROR);
     return FALSE;
  }
    
  // we need a persistant connection for prepared statements
  $sql = "SELECT ".
         "  NME.networkedge_id, " .
         "  F1.feature_id as node1, FS1.name as landmark1, FL1.fmin as fmin1, FL1.fmax as fmax1, ".
         "  F2.feature_id as node2, FS2.name as landmark2, FL2.fmin as fmin2, FL2.fmax as fmax2, ". 
         "  NE.weight " .
         "FROM networkmod_edge NME " .
         "  INNER JOIN networkedge NE       ON NME.networkedge_id = NE.networkedge_id " .
         "  INNER JOIN feature F1           ON NE.node1 = F1.feature_id " .
         "  INNER JOIN feature F2           ON NE.node2 = F2.feature_id " .
         "  INNER JOIN networkmod NM        ON NME.networkmod_id = NM.networkmod_id " .
         "  INNER JOIN featureloc FL1       ON FL1.feature_id = F1.feature_id " .
         "  INNER JOIN feature FS1          ON FS1.feature_id = FL1.srcfeature_id " .
         "  INNER JOIN featureloc FL2       ON FL2.feature_id = F2.feature_id " .
         "  INNER JOIN feature FS2          ON FS2.feature_id = FL2.srcfeature_id " .
         "  INNER JOIN cvterm CVTs1         ON FS1.type_id = CVTs1.cvterm_id " .
         "  INNER JOIN cvterm CVTs2         ON FS2.type_id = CVTs2.cvterm_id " .
  	     "WHERE " .
         "  NM.networkmod_id = %d and CVTs1.name = '%s' and CVTs2.name = '%s'";
  $module_edges = chado_query($sql, $module_id, $so_type, $so_type);  	
  
  
  // open the configuration file 
  $conf_file = "$circos_exec_dir/edges.$module_id.conf";
  $cfh = fopen($conf_file, 'w');
  
  // iterate through the edges and add them to the config file
  while ($edge = db_fetch_object($module_edges)) {
    fwrite($cfh, $edge->networkedge_id . " " . $edge->landmark1 . " " . $edge->fmin1 . " " . $edge->fmax1 . "\n");
    fwrite($cfh, $edge->networkedge_id . " " . $edge->landmark2 . " " . $edge->fmin2 . " " . $edge->fmax2 . "\n");
  }  
  fclose($cfh); 

  return $conf_file;
}

/**
 * 
 * @ingroup tripal_network
 */
function tripal_network_write_circos_config($karyotype_file, $edge_file,
  $tiles, $module_id, $circos_exec_dir) {
  	
  if(!$module_id) {
     watchdog('tripal_network', "Cannot create circos config file due to missing module ID", array(), WATCHDOG_ERROR);
     return FALSE;
  }
  
  if(!file_exists($karyotype_file)){
  	watchdog('tripal_network', "Karyotpe file does not exists: '%file'", array('%file' => $karyotype_file), WATCHDOG_ERROR);
  	return FALSE;
  }
  
  if(!file_exists($edge_file)){
    watchdog('tripal_network', "Edge file does not exists: '%file'", array('%file' => $edge_file), WATCHDOG_ERROR);
    return FALSE;
  }
  
  // create a name for the output image file
  $outfile = "module.$module_id";

  $conf_file = "$circos_exec_dir/circos.conf";
  $cfh = fopen($conf_file, 'w');
  
  // write the circos configuration file
	fwrite($cfh, "
		<colors>
		   <<include etc/colors.conf>>
		   <<include etc/colors.unix.txt>>
		</colors>
		
		<fonts>
		   <<include etc/fonts.conf>>
		</fonts>
		
		<ideogram>
		   <spacing>
		      default = 0.01r
		      break   = 1u
		      axis_break_at_edge = yes
		      axis_break         = yes
		      axis_break_style   = 2
		      <break_style 1>
		         stroke_color = black
		         fill_color   = blue
		         thickness    = 0.25r
		         stroke_thickness = 2
		      </break>
		      <break_style 2>
		         stroke_color     = black
		         stroke_thickness = 3
		         thickness        = 1.5r
		      </break>
		   </spacing>
		
		   # thickness (px) of chromosome ideogram
		   thickness        = 15p
		   stroke_thickness = 2
		   
		   # ideogram border color
		   stroke_color     = black
		   fill             = yes
		   
		   # the default chromosome color is set here and any value
		   # defined in the karyotype file overrides it
		   fill_color       = black
		
		   # fractional radius position of chromosome ideogram within image
		   radius         = 0.6r
		   show_label     = yes
		   label_with_tag = yes
		   label_font     = condensedbold
		   label_radius   = dims(ideogram,radius) + 0.3r
		   label_size     = 16p
		
		   # cytogenetic bands
		   band_stroke_thickness = 2
		
		   # show_bands determines whether the outline of cytogenetic bands
		   # will be seen
		   show_bands            = yes
		   
		   # in order to fill the bands with the color defined in the karyotype
		   # file you must set fill_bands
		   fill_bands            = yes
		</ideogram>
		
		show_ticks          = yes
		show_tick_labels    = yes
		
		<ticks>
		   radius               = dims(ideogram,radius_outer)
		   label_multiplier     = 1e-6
		   orientation          = out
		   <tick>
		      spacing        = 5000000u
		      size           = 3p
		      thickness      = 1p
		      color          = black
		      show_label     = yes
		      label_size     = 10p
		      label_offset   = 5p
		      format         = %d Mb
		   </tick>
		</ticks>
		
		karyotype = $karyotype_file
		
		<links>
		   z      = 0
		   radius = 0.99r
		   bezier_radius = 0r
		   crest = 0.5
		   <link edges>
		      z            = 1
		      show         = yes
		      color        = gray
		      thickness    = 1
		      file         = $edge_file
		      record_limit = 50000
		   </link>
		</links>
	");
	
  if ($tiles) {
    foreach ($tiles as $i => $tile) {
      fwrite($cfh,  "
  			<plots>
  			   layers_overflow  = hide
  			   layers           = 10
  			   margin           = 0.02u
  			   thickness        = 5
  			   padding          = 8
  			   orientation      = in
  			   stroke_thickness = 1
  			   stroke_color     = black
  			   show             = yes
  			
  			   <plot>
  			      type             = tile
  			      file             = $tile
  			      r1               = 0.98r
  			      r0               = 0.60r
  			   </plot>
  			</plots>
  	  ");
    }
  }
  fwrite($cfh,  "
		<image>
		  dir   = .
		  file  = $outfile.png
		  png   = yes
		  # radius of inscribed circle in image
		  radius         = 300p
		
		  # by default angle=0 is at 3 o'clock position
		  angle_offset      = -90
		
		  #angle_orientation = counterclockwise
		
		  auto_alpha_colors = yes
		  auto_alpha_steps  = 5
		
		  background = white
		
		</image>
		
		# RGB/HSV color definitions, color lists, location of fonts, fill patterns.
		# Included from Circos distribution.
		<<include etc/colors_fonts_patterns.conf>>
		
		# Debugging, I/O an dother system parameters
		# Included from Circos distribution.
		<<include etc/housekeeping.conf>>
  ");
  fclose($cfh);
  
  return $conf_file;
}