//var num_chrs = 12;
//var gap_bp_size = 5000000; // gap size in bp
//var total_size = 372317567 + (num_chrs * gap_bp_size);
//var gap_radians = (gap_bp_size / total_size) * 2 * Math.PI;
//var total_radians = 2 * Math.PI;
//var radian_per_bp = total_radians / total_size;

//image size
//var width = 600;
//var height = 600;

// position by radius of inner and outer arcs of chromosomes
//var innerRadius = Math.min(width, height) * .30;
//var outerRadius = innerRadius * 1.1;

// ticks
//var tick_bp_spacing = 5000000; 
//var radian_per_tick = (tick_bp_spacing / total_size) * 2 * Math.PI;

function init_genome_viewer() {
  
  //-----------------------
  // setup the visualizer
  var vis = d3.select("#genome-view").append("svg")  
     .attr("width", width ? width : 0)
     .attr("height", height)   
       .append("g").attr("class", "base")
     .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
  
  // add the three groups: chromsomes, edges, trait experiments
  vis.append("g").attr("class", "arcs");
  vis.append("g").attr("class", "edges");
  vis.append("g").attr("class", "trait-edges");
  vis.append("g").attr("class", "trait-exps");
  
  // add a tooltip we can use for popup info
  var tooltip = d3.select("body")
    .append("div")
    .attr("class", "d3-tooltip")
    .style("position", "absolute")
    .style("z-index", "10")
    .style("visibility", "hidden")    
    .style("border", "1px solid #000000")
    .style("background-color", "#FFFFFF")
    .style("padding", "10px")
  
}
/*
 * 
 */
function genome_viewer_add_landmarks(chr_sizes) {
  //-----------------------
  // add arcs
  // create the arc generator
  var arc = d3.svg.arc() 
    .outerRadius(outerRadius)
    .innerRadius(innerRadius)
    .startAngle(function(d) {
      return calculate_radii (chr_sizes, d.label, 0, 0);
    })
    .endAngle(function(d) {     
      return calculate_radii (chr_sizes, d.label, d.value, 0);
    });
    
  // select the <g class="base"> element and add a new 
  // child <g class="arc"> element for each entry in the data.
  var arcs = d3.select("g.arcs")
    .selectAll("g")
    .data(chr_sizes)  
    .enter().append("g")
    .attr("class", "arc");
  
  // for each arc (or <g class="arc"> element) add a <path> element
  // where the path data <d> is the arc data
  arcs.append("path")
    .style("fill", function(d) { return d.color; })
    .style("stroke", "black")
    .attr("d", arc)   // path 'd' attribute contains the path data
    .text(function(d) { return d.label; });   
  
  // -----------------
  // add arc labels    
  arcs.append("svg:text")
      .attr("transform", function(d) { 
        var c = arc.centroid(d);
        return "translate(" + (c[0] * 1.5) + "," + (c[1] * 1.5) + ")"; 
      })
      .attr("text-anchor", "middle")
      .style("font-weight", "bold")
      .text(function(d) { return d.label; }); 
   
  //-----------------------
  // add ticks  
  // for each arc add a new <g> element to house all of the 
  // ticks for each arc.
  var ticks = arcs.append("g")  // append a new <g> tag
    .selectAll("g")             // select all child <g> tags (non exist)
    .data(chr_ticks)            // associate data with this selection
    .enter().append("g")        // for each tick mark add a new <g> tag.
    .attr("transform", function(d) {
      // move the tick from the center to the outerRadius
      // and rotate the desired amount
      transform = "rotate(" + d.angle + ") translate(" + outerRadius + ", 0)";
      return transform
    });    

  ticks.append("line")
      .attr("x1", 1)
      .attr("y1", 0)
      .attr("x2", 5)
      .attr("y2", 0)
      .style("stroke", "#000");
  
  ticks.append("text")
      .attr("x", 8)
      .attr("dy", ".35em")
      .attr("text-anchor", function(d) {
        return 0;
      })
      .text(function(d) { return d.label; });
  
  // the chr_ticks function generates an array of radians
  // and labels for each ticks.  The radian indcates the 
  // rotation of the tick and the label is the text adjacent to
  // the tick
  function chr_ticks(d) { 
    start = calculate_radii (chr_sizes, d.label, 0, 0);
    end = calculate_radii (chr_sizes, d.label, d.value, 0);
    var ticks = d3.range(start, end, radian_per_tick).map(function(v, i) {
      return {
        angle:  Math.round(((v / (2 * Math.PI)) * 360) - 90),
      label: Math.round((((v - start) / radian_per_tick) * tick_bp_spacing) / 1000000) + " Mbp",
      };
    })
    return ticks;
  }
}
/*
 * 
 */
function genome_viewer_add_edges(chr_sizes, netedges, type) {
  // d[0] contains the landmark
  // d[1] contains the position
  // each edge has three points:  start, (0,0), and end.  
  // the 0,0 is to give the edge a nice bezier curve around
  
  //---------------------
  // add network edges     
  var line = d3.svg.line()
    .x(function(d){
      if (d[0] == 0) {
        return 0;
      }
      var a = calculate_radii(chr_sizes, d[0], d[1], -Math.PI/2);
      return innerRadius * Math.cos(a);
    })
    .y(function(d){
      if (d[0] == 0) {
        return 0;
      }
      var a = calculate_radii(chr_sizes, d[0], d[1], -Math.PI/2);
      return innerRadius * Math.sin(a);
    })
    .interpolate("basis");
  
  // find the <g class="edges"> element and add in
  // new <g class="diagonal"> elements with the path
  // for the edges
  if(type == 'edges') {
    var edges = d3.select("g.edges")
      .selectAll("g")
      .data(netedges)  
      .enter().append("g")
      .attr("class", "line");
    
    edges.append("path")
      .style("stroke", "black")
      .style("stroke-opacity", "0.3")
      .style("fill-opacity", 0)
      .attr("d", line);  
  }
  if(type == 'trait-edges') {
    var edges = d3.select("g.trait-edges")
    .selectAll("g")
    .data(netedges)  
    .enter().append("g")
    .attr("class", "line");
  
  edges.append("path")
    .style("stroke", "red")
    .style("stroke-opacity", "0.3")
    .style("fill-opacity", 0)
    .attr("d", line); 
  }
}
/*
 *  
 */ 
function genome_viewer_add_trait_experiments(chr_sizes, traits) {
  var startRadius = innerRadius - 10;
  var curr_radius = startRadius;
  var colors = [];
  
  // add an index and a color to the traits array
  for (i = 0; i < traits.length; i++) {
    traits[i].index = i;
    traits[i].fmax = parseInt(traits[i].fmax);
    traits[i].fmin = parseInt(traits[i].fmin);
    colors[traits[i].trait_name + ":" + traits[i].map_name] = '#' + Math.floor(Math.random()*16777215).toString(16);
  }
  
  //-----------------------
  // add arcs
  // create the arc generator
  var arc = d3.svg.arc() 
    .innerRadius(function(d) {
      var depth = 0;
      // the exps should be provided as sorted list from
      // largest to shorted size.  Otherwise this code 
      // won't work
      for (i = 0; i < traits.length; i++) {
        if (d.index == i) {
          break;
        }
        if (traits[i].landmark == d.landmark &&
            ((traits[i].fmin >= d.fmin && traits[i].fmax <= d.fmax) ||
             (traits[i].fmin <= d.fmin && traits[i].fmax >= d.fmax) ||
             (traits[i].fmax <= d.fmax && traits[i].fmax >= d.fmin) ||
             (traits[i].fmin >= d.fmin && traits[i].fmin <= d.fmax))){          
          depth++;          
        }
      }
      var radius = startRadius - (depth * 7);      
      if(radius < 10) {
        radius = 10;
      }
      curr_radius = radius;
      return radius;
    })
    .outerRadius(function(d) {
      radius = curr_radius + 5;
      curr_radius = startRadius;
      return radius;
    })
    .startAngle(function(d) { 
      return  calculate_radii(chr_sizes, d.landmark, d.fmin, 0); 
    })
    .endAngle(function(d) {
      return  calculate_radii(chr_sizes, d.landmark, d.fmax, 0);
    }); 
  
  var arcs = d3.select("g.trait-exps")
    .selectAll("g")
    .data(traits)  
    .enter().append("g")
    .attr("class", "exp-arc");

  // for each arc (or <g class="arc"> element) add a <path> element
  // where the path data <d> is the arc data
  arcs.append("path")
    .style("fill", function(d) { return colors[d.trait_name + ":" + d.map_name]; })
    .style("stroke", 'black')
    .style("stroke-opacity", "0.7")
    .style("fill-opacity", "0.7")
    .attr("d", arc)   // path 'd' attribute contains the path data
    .text(function(d) { return d.trait; })
    .on("mouseover", function(){
      tooltip = d3.select(".d3-tooltip");
      return tooltip.style("visibility", "visible");
    })
    .on("mousemove", function(){
      tooltip = d3.select(".d3-tooltip");
      tooltip.text(this.parentNode.text);
      return tooltip.style("top", window.mouseYPos + 'px').style("left", window.mouseXPos + 10 + 'px');
    })
    .on("mouseout", function(){
      tooltip = d3.select(".d3-tooltip");
      return tooltip.style("visibility", "hidden");
    });
}
/*
 * 
 */
function calculate_radii (chr_sizes, landmark, position, offset) {
  var lbp = 0;  // the landmark start bp position (summation of all chromosomes)
  for (i = 0; i < chr_sizes.length; i++) {
    if (chr_sizes[i].label == landmark) {      
      break;
    }
    lbp += chr_sizes[i].value + gap_bp_size;
  }
  var a = (((lbp + parseInt(position)) / total_size) * 2 * Math.PI) + offset;
  return a;
}
