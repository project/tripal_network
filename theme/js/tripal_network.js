if (Drupal.jsEnabled) {
  $(document).ready(function() {       
    
    // these global variables are used for dragging of an element.
    var drag_startX = 0;
    var drag_startY = 0;
    var element_top = 0;
    var element_left= 0;
    var mouse_down  = 0;
    var element = 0;
  
    // variable for holding network json
    var network_arr;
  
    // set the mouse position
    $().mousemove(function(e){
       window.mouseXPos = e.pageX;
       window.mouseYPos = e.pageY;
    });
    $().mousedown(function(){
      mouse_down = 1;
      drag_startX = window.mouseXPos;
      drag_startY = window.mouseYPos; 
      // the element that get's the mouse down
      // needs to set the element, element_top
      // and element_left globals.
    });
    $().mouseup(function(){
      mouse_down = 0;
      element = 0;
    });
    $().mousemove(function(){       
      if (mouse_down && element) {
        var dx = drag_startX - window.mouseXPos;
        var dy = drag_startY - window.mouseYPos;            
        var top = element_top - dy;
        var left = element_left - dx;
                    
        $(element).css({
          'top': top + 'px', 
          'left': left + 'px',
        });
      }
    });
    
    // if the tripal_network-explorer-box is present on the page then
    // do the rest
    if ($('#tripal_network-explorer-box').length != 0) {
      // initalize the genome viewer
      init_genome_viewer();
      genome_viewer_add_landmarks(chr_sizes);
    
      // hide the content
      $('#tripal-module-explorer-function').hide();
      $('#tripal-module-explorer-genome').hide();
      $('#tripal-module-explorer-edges').hide();
      $('#tripal-module-explorer-nodes').hide();
      $('#tripal-module-explorer-trait').hide();
      $('#tripal-module-explorer-genetics').hide();
      $('#tripal-module-explorer-conservation').hide();
      $('#tripal-module-explorer-markers').hide();
  
      // set the first tab as active
      $('#tripal-module-explorer-menu li').css('background-color','#AAAAAA');
      $('.first-item').css('background-color','#EEEEEE');
      
      // set the on change functions for the various form elements
      $('#edit-module').change(function(){
        load_module_data();
      });
      
      $('#edit-types').change(function(){
        load_traits();
      });
    
      $('#edit-traits').change(function(){
        load_traits();        
      });
      
      $('#edit-filterbutton').click(function(){
        // we need to clean up any bio markers from a previous filter set.
        $("#marker-info").fadeOut(100).html('').fadeIn(500);
        
        // reload the tabs
        load_module_data(); 
        
        return false;
      });
      
      $('#edit-flankbutton').click(function(){      
        load_biomarkers();
        return false;
      });           
      
      // reload the module details if a filter is applied
      $('#edit-button').click(function(){
        load_module_data();
        return 0;
      });
      
      // if we have incoming parameters then load the fields with those first
      var query = getUrlVars();
      
      var module_id = query['module_id'];
      if(module_id != null){
        $("#edit-module").val(module_id);
      }
      
      var overlaps = query['overlaps'];
      if(overlaps != null){
        $("#edit-overlaps").val(overlaps);
      } 
      
      var types = query['types'];
      if(types != null){
        $("#edit-types").val(types);
      }
      
      var traits = query['traits'];
      if(traits != null){
        $("#edit-traits").val(traits);
      }
      
      // if we have a module_id then load the data and scroll to the explorer tabs
      if(module_id != null){
        load_module_data();
        $('html, body').animate({scrollTop: $("#edit-module").offset().top}, 2000);
      }
      
      // when clicking on the menu tabs change the content that is displayed
      // and make the selected tab a brighter color
      $('#tripal-module-explorer-menu li').click(function(){ 
         // hide all content
         $('.tripal-module-explorer-item').hide();
         // set background color to dfault for all menu tabs
         $('#tripal-module-explorer-menu li').css('background-color','#AAAAAA');
         // show the content for the clicked tab
         item = $(this).attr('id');
         item = item.replace("-tab","");
         $(item).show();
         // set the background color for the clicked tab to be lighter
         $(this).css('background-color','#EEEEEE');
      });
    }
    
    //-------------------------------------------------------------------------
    // this function is used to get query arguments
    function getUrlVars() {
      var vars = [];
      var pairs = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
      for(var i = 0; i < pairs.length; i++) {
        pair = pairs[i].split('=');
        key = pair[0];
        value = pair[1];
        
        if(vars[key] == undefined) {
          vars[key] = value;
        }
        // if the key has been used before then convert to hash.
        else if(vars[key] != undefined && !$.isArray(vars[key])) {
          prev = vars[key];
          vars[key] = [];
          vars[key].push(prev);
          vars[key].push(value);
        }
        else {
          vars[key].push(value);
        }          
      }
      return vars;
    }
    
    
    //-------------------------------------------------------------------------
    // this function is called after a selection is made for a module or cluster
    // it retrieves the network in JSON from from the tripal_network module.
    function load_module_data(){
      load_cytoscape();
      load_edges();
      load_nodes();
      load_function();
      load_traits();
      load_genetic_features();
    }
    
    function load_cytoscape(){
      var module_id     = $("select#edit-module").val();
      var types         = $("select#edit-types").val();
      var traits        = $("select#edit-traits").val();
      var maps          = $("select#edit-maps").val();
      var overlaps      = $("#edit-overlaps").val();
      
      // draw the network graph 
      $("#cytoscape-loading").show();
      $.ajax({ 
        url: baseurl + "/network/cytoscape", 
        dataType: 'json',
        data : {module_id : module_id, 
                traits: traits,
                maps: maps,
                types: types,
                overlaps: overlaps},
        success: function(json){          
          network_arr = json;
          draw_graph(network_arr, 'ForceDirected');
          $("#cytoscape-loading").hide();
        }
      });
    }

    
    function load_edges() {
      var module_id     = $("select#edit-module").val();
      var types         = $("select#edit-types").val();
      var traits        = $("select#edit-traits").val();
      var maps          = $("select#edit-maps").val();
      var overlaps      = $("#edit-overlaps").val();
      
      // get module edges  
      $("#edges-loading").show();
      $("#genome-loading").show();
      $.ajax({ 
        url: baseurl + "/network/module_edges", 
        dataType: 'json',
        data : {module_id : module_id, 
                traits: traits,
                maps: maps,
                types: types,
                overlaps: overlaps},
        success: function(json){
          
          // now that we have our edges we can build the genome viewer too.
          $("g.edges g").remove();
          $("g.trait-edges g").remove();
          genome_viewer_add_edges(chr_sizes, json['d3list'], 'edges');
          genome_viewer_add_edges(chr_sizes, json['d3list_traits'], 'trait-edges');
          $("#genome-loading").hide();
          
          $("#edge-info").fadeOut(100).html(json['table']).fadeIn(500);
          $("#edges-loading").hide();
          
        }
      });
    }

    function load_nodes() {
      var module_id     = $("select#edit-module").val();
      var types         = $("select#edit-types").val();
      var traits        = $("select#edit-traits").val();
      var maps          = $("select#edit-maps").val();
      var overlaps      = $("#edit-overlaps").val();      
      
      // get module nodes
      $("#nodes-loading").show();
      $.ajax({ 
        url: baseurl + "/network/module_nodes", 
        dataType: 'json',
        data : {module_id : module_id, 
                traits: traits,
                maps: maps,
                types: types,
                overlaps: overlaps},
        success: function(json){
          $("#node-info").fadeOut(100).html(json['nodes_table']).fadeIn(500);          
          $("#nodes-loading").hide();          
        }
      });
    }
    
    function load_function() {
      var module_id     = $("select#edit-module").val();
      
      // get module function    
      $("#function-loading").show();
      $.ajax({ 
        url: baseurl + "/network/module_enrichment", 
        dataType: 'json',
        data : {module_id : module_id},
        success: function(json){
          $("#enrich-info").fadeOut(100).html(json['table']).fadeIn(500);
          $("#function-loading").hide();
        }
      });
    }
    
    function load_traits() {
      var module_id     = $("select#edit-module").val();
      var types         = $("select#edit-types").val();
      var traits        = $("select#edit-traits").val();
      var maps          = $("select#edit-maps").val();
      var overlaps      = $("#edit-overlaps").val();
    
      // get trait data for genome viewer
      $("#trait-loading").show();
      $.ajax({ 
        url: baseurl + "/network/module_traits", 
        dataType: 'json',
        data : {module_id : module_id, 
                traits: traits,
                maps: maps,
                types: types,
                overlaps: overlaps},
        success: function(json){
          // remove the element in the SVG that houses the traits and
          // then readd them to match the resturned results
          $("g.trait-exps g").remove();
          genome_viewer_add_trait_experiments(chr_sizes, json['positions']);

          // update the select filter dropdowns
          $("#edit-traits").fadeOut(100).html(json['trait_select']).fadeIn(500);
          $("#edit-maps").fadeOut(100).html(json['map_select']).fadeIn(500);                       
          
          // set the trait report table
          $("#trait-loading").hide();         
          
        }
      });
    }
    
    function load_genetic_features() {
      var module_id     = $("select#edit-module").val();
      var types         = $("select#edit-types").val();
      var traits        = $("select#edit-traits").val();
      var maps          = $("select#edit-maps").val();
      var overlaps      = $("#edit-overlaps").val();
    
      // get trait data for genome viewer
      $("#genetics-loading").show();
      $.ajax({ 
        url: baseurl + "/network/genetic_features", 
        dataType: 'json',
        data : {module_id : module_id, 
                traits: traits,
                maps: maps,
                types: types,
                overlaps: overlaps},
        success: function(json){    
          $("#genetics-info").fadeOut(100).html(json['table']).fadeIn(500);
          $("#genetics-loading").hide();
        }
      });
    }
    
    function load_biomarkers() {
      var module_id     = $("select#edit-module").val();
      var types         = $("select#edit-types").val();
      var traits        = $("select#edit-traits").val();
      var maps          = $("select#edit-maps").val();
      var overlaps      = $("#edit-overlaps").val();
      var flanking      = $("#edit-flanking").val();
    
      // get trait data for genome viewer
      $("#markers-loading").show();
      $.ajax({ 
        url: baseurl + "/network/biomarkers", 
        dataType: 'json',
        data : {module_id : module_id, 
                traits: traits,
                maps: maps,
                types: types,
                overlaps: overlaps,
                flanking: flanking},
        success: function(json){    
          $("#marker-info").fadeOut(100).html(json['markers_table']).fadeIn(500);
          $("#markers-loading").hide();
        }
      });
    }
  
    //-------------------------------------------------------------------------
    // After the network JSON is obtained it is passed to this function for
    // drawing inside of the 'cytoscape-viewer' div object.
    function draw_graph(network_json, layout_type){    
  
      var div_id = "cytoscapeweb";
  
      // initialization options
      var options = {
        swfPath: "/sites/all/modules/tripal_network/cytoscapeweb/swf/CytoscapeWeb",
        flashInstallerPath: "/sites/all/modules/tripal_network/cytoscapeweb/swf/playerProductInstall"
      };
  
      var vis = new org.cytoscapeweb.Visualization(div_id, options);
  
      // callback when Cytoscape Web has finished drawing
      vis.ready(function() {
        
        // when a node is clicked, take the user to the node page
        vis.addListener("dblclick", "nodes", function(event) {           
          var target = event.target;          
          nid = target.data['nid'];
          url = baseurl + "/node/" + nid;
          window.open(url, '_blank', '');         
        });
          
        // when clicked a node display node details
        vis.addListener("click", "nodes", function(event) {       
          var target = event.target;
          var mouseX = window.mouseXPos;
          var mouseY = window.mouseYPos;
            
          nid = target.data['nid'];
          url = baseurl + "/node/" + nid;
          id = target.data['id'];
            
          $('#cytoscape-node-details').css({
            'top': mouseY + 'px', 
            'left': mouseX + 'px',
          });
            
          // call an ajax function to get the node annotations
          $("#term-table").hide();
          $('#term-table-loading').show();
          $('#cytoscape-node-label').hide(); // if the label is showing then hide it
          $('#cytoscape-node-details').show();          
          $.ajax({ 
            url: baseurl + "/network/node_annotations", 
            dataType: 'json',
            data : {node : target.data['id']},
            success: function(json){
              new_html = "<p><a href=\"" + url + "\">" + target.data['label'] + "</a></p><p><i>Click and drag to move this box</i></p>" +  
                json['terms'] + 
                "<br>" + 
                json['properties'];
              $('#term-table-loading').hide();
              $("#term-table").html(new_html).fadeIn(500);
            }
          });
        });
          
        // allow the person to move the box when clicking and dragging
        $('#cytoscape-node-details').mousedown(function(){          
          element = $('#cytoscape-node-details');
          element_top = parseInt($('#cytoscape-node-details').css('top').replace('px',''));
          element_left = parseInt($('#cytoscape-node-details').css('left').replace('px',''));          
        });
          
        $("#node-details-close").click(function() { 
          $('#cytoscape-node-details').hide()
        });
          
          
        // when moused over a node display node details
        vis.addListener("mouseover", "nodes", function(event) { 
            
          var target = event.target;
          var mouseX = window.mouseXPos;
          var mouseY = window.mouseYPos;            
          label = target.data['label'];          
              
          $('#cytoscape-node-label').css({
            'top': mouseY + 'px', 
            'left': mouseX + 'px',
          });
          $('#cytoscape-node-label').html(label);
          $('#cytoscape-node-label').fadeIn(500);
        })
          
        // when moused out of a node hide the display label
        vis.addListener("mouseout", "nodes", function(event) {    
          $('#cytoscape-node-label').hide();
        })
      });
  
      // visual style we will use
      var visual_style = {
        global: {
          backgroundColor: "#FFFFFF"
        },
        nodes: {
          shape: "CIRCLE",
          borderWidth: 1,
          borderColor: "#000000",
          size: {
            defaultValue: 25,
            continuousMapper: { attrName: "weight", minValue: 25, maxValue: 75 }
          },
          //color: { passthroughMapper: { attrName: "color" } },
          color: {
            discreteMapper: {
              attrName: "has_trait",
              entries: [
                  { attrValue: "0", value: "#94ba46" },
                  { attrValue: "1", value: "#d0423f" },
              ]
            }
          },
          labelHorizontalAnchor: "top",
          selectionColor: "#FFFF00",
        },
          
        edges: {
          width: 1,
          color: {
            discreteMapper: {
              attrName: "neg",
              entries: [
                  { attrValue: 0, value: "#000000" },
                  { attrValue: 1, value: "#0000FF" },
              ]
            }
          },
          opacity: 0.5,
          width: 2,
        }
      };
      var layout = {
        name: layout_type,
      };
      var draw_options = {
        network: network_json,
        panZoomControlVisible: true, 
        visualStyle: visual_style,
        nodeLabelsVisible: true,
        layout: layout
      };
  
      vis.draw(draw_options);
    }
  });
}


    
