<?php
$feature = $variables['node']->feature;

// expand the feature object to include the libraries from the network_feature
// table in chado.
$options = array('return_array' => 1);
$feature = tripal_core_expand_chado_vars($feature, 'table', 'networkmod_node', $options);
$networkmod_nodes = $feature->networkmod_node;

// don't show this page if there are no libraries
if (count($networkmod_nodes) > 0) { ?>
  <div id="tripal_feature-network_modules-box" class="tripal_feature-info-box tripal-info-box">
    <div class="tripal_feature-info-box-title tripal-info-box-title">Network Modules</div>
    <div class="tripal_feature-info-box-desc tripal-info-box-desc">This <?php print $feature->type_id->name ?> is part of the following network modules</div>
    <table id="tripal_feature-network_modules-table" class="tripal_feature-table tripal-table tripal-table-horz">
      <tr>
        <th>Network Name</th>
        <th>Module Name</th>
        <th>Edge Count</th>
        <th>Node Count</th>
        <th>Average Degree &lt;<i>k</i>&gt;</th>
      </tr> <?php
      $i = 0; 
      foreach ($networkmod_nodes as $networkmod_node) {
        $class = 'tripal-table-odd-row';
        $networkmod = $networkmod_node->networkmod_id;          
        $network    = $networkmod_node->networkmod_id->network_id;
        
        // get the module properties
        $properties = array();
        $properties['node_count'] = 0;
        $properties['edge_count'] = 0;
        $properties['average_degree'] = 0;
        tripal_network_module_topological_properties($networkmod->networkmod_id, $properties);
        $node_count = $properties['node_count'];
        $edge_count = $properties['edge_count'];
        $avg_degree = $properties['average_degree'];
        
        // now build the table row
        if ($i % 2 == 0 ) {
           $class = 'tripal-table-even-row';
        } ?>
        <tr class="<?php print $class ?>">
          <td><?php 
            if ($network->nid) {
               print "<a href=\"". url("node/".$network->nid) . "\">" . $network->name . "</a>";
            } else {
               print $network->name;
            } ?>
          </td>
          <td><?php 
           if ($network->nid) {
               print "<a href=\"". url("node/".$network->nid) . "?block=explorer&module_id=$networkmod->networkmod_id\" target=\"_blank\">" . $networkmod->name . "</a>";
            } else {
               print $networkmod->name;
            } ?>
          </td>
          <td><?php print number_format($edge_count) ?></td>
          <td><?php print number_format($node_count) ?></td>
          <td><?php printf("%.2f", $avg_degree);?></td>
        </tr> <?php
        $i++;  
      } ?>
    </table> 
  </div><?php 
} 

