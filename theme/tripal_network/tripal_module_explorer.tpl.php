<?php

// get the network and it's description property
$network  = $variables['node']->network;

$module_select_form = $variables['module_select_form'];    
$trait_select_form  = $variables['trait_select_form'];  
$flanking_markers_form = $variables['flanking_markers_form'];  
$has_modules = $variables['has_modules']; 

if ($has_modules) { ?>
  <div id="tripal_network-explorer-box" class="tripal_network-info-box tripal-info-box">
    <div class="tripal_network-info-box-title tripal-info-box-title">Module Explorer</div>
    <div class="tripal_network-info-box-desc tripal-info-box-desc">Select a module from the 
      list below to view its details.  The number in parentheses is the number of nodes in the module. </div>
    <div id="tripal_ajaxLoading" style="display:none">
      <div id="loadingText">Loading...</div>      
    </div>   
    <?php print $module_select_form ?> 
    
    <div id="tripal-module-explorer-menu">
      <ul>
        <li id="#tripal-module-explorer-cytoscape-tab" class="first-item"><span>Module</br>View</span></li>
        <li id="#tripal-module-explorer-genome-tab"><span>Genome</br>View</span></li>
        <li id="#tripal-module-explorer-edges-tab"><span>Edges</br>&nbsp;</span></li>
        <li id="#tripal-module-explorer-nodes-tab"><span>Nodes</br>&nbsp;</span></li>
        <li id="#tripal-module-explorer-function-tab"><span>Functional</br>Enrichment</span></li>
        <li id="#tripal-module-explorer-genetics-tab"><span>Genetic</br>Features</span></li>        
        <li id="#tripal-module-explorer-trait-tab"><span>Filter By</br>Trait</span></li>        
        <li id="#tripal-module-explorer-markers-tab"><span>Potential</br>Bio Markers</span></li>        
        <!--<li id="#tripal-module-explorer-conservation-tab"><span>Conservation</br>&nbsp;</span></li>-->
      </ul>      
    </div>
    <div id="tripal-module-explorer-content">
      <div id="tripal-module-explorer-cytoscape" class="tripal-module-explorer-item">
         <b>Network Viewer</b> 
         <div id="cytoscape-loading" class="tripal-network-loading-box" style="display: none"><p>Loading...</br><img src="<?php print $base_url . "/" . drupal_get_path('module', 'tripal_network') . '/theme/images/ajax-loader.gif' ?>"></p></div>
         <div id="cytoscape-node-details" style="display:none">
           <div id="node-details-close"><a name="close">close [x]</a></div>
           <div id="term-table-loading" style="display: none"><img src="<?php print $base_url . "/" . drupal_get_path('module', 'tripal_network') . '/theme/images/ajax-loader.gif' ?>"></div>
           <div id="term-table"></div>
         </div>
         <div id="cytoscape-node-label" style="display:none"></div>
         <br>The viewer is interactive. You can drag-and-drop nodes and zoom in and out using the 
         navigation tool on the bottom right.  Click a node for more details. 
         <b><i>Note:</i></b> Large modules may take a while to display.
         <div id="cytoscapeweb"></div><br>
         <div style="float:right; margin-left: 10px; margin-botton: 10px"><i>Visualized Using</i><br>
         <a href="http://cytoscapeweb.cytoscape.org/">
           <img src="http://cytoscapeweb.cytoscape.org/img/logos/cw_s.png" alt="Cytoscape Web"/>
         </a></div>
         <b>Legend</b>
         <ul>
           <li>Red Nodes: When filtering the module
               by trait, the nodes adjacent to or contained by genetic features</li>
           <li>Blue Edges: Inversely correlated relationship.</li>
         </ul>
      </div>
      <div id="tripal-module-explorer-genome" class="tripal-module-explorer-item">
         <b>Genome View</b>
         <div id="genome-loading" class="tripal-network-loading-box" style="display: none"><p>Loading...</br><img src="<?php print $base_url . "/" . drupal_get_path('module', 'tripal_network') . '/theme/images/ajax-loader.gif' ?>"></p></div>
         <br>The network visualized using a circular genome plot. When filtering the module by trait,
         the edges connecting nodes adjacent to or contained by genetic features are colored red.
         <div id="genome-view"></div>
         <p>When traits are selected, red edges are those where at least one node in the edge is contained within (QTL) or adjacent to (SNP) a genetic feature associated with the selected trait.</p>
       </div>       

       <div id="tripal-module-explorer-edges" class="tripal-module-explorer-item">
         <b>Network Edges</b>
         <div id="edges-loading" class="tripal-network-loading-box" style="display: none"><p>Loading...</br><img src="<?php print $base_url . "/" . drupal_get_path('module', 'tripal_network') . '/theme/images/ajax-loader.gif' ?>"></p></div>
         <br><i>The following edges exist in this module. When filtering the module by trait, this
         report will change to indicate which edges overlap with the genetic features selected.</i>
         <div id="edge-info"><br><b>Please select a Module</b></div>
       </div>  

       <div id="tripal-module-explorer-nodes" class="tripal-module-explorer-item">
         <b>Module Nodes</b>
         <div id="nodes-loading" class="tripal-network-loading-box" style="display: none"><p>Loading...</br><img src="<?php print $base_url . "/" . drupal_get_path('module', 'tripal_network') . '/theme/images/ajax-loader.gif' ?>"></p></div>
         <br><i>The following nodes exist in this module.  Annotations assigned to each node are listed.
           This list does not change as traits are filtered.</i>
         <div id="node-info"><br><b>Please select a Module</b></div>
       </div>  

       <div id="tripal-module-explorer-function" class="tripal-module-explorer-item">
         <b>Function</b>
         <div id="function-loading" class="tripal-network-loading-box" style="display: none"><p>Loading...</br><img src="<?php print $base_url . "/" . drupal_get_path('module', 'tripal_network') . '/theme/images/ajax-loader.gif' ?>"></p></div>
         <br><i>The following terms are enriched, or over-represented in comparison to the genome backgroud,
           within the selected module.  
           The p-values associated with these terms were calculated in a prior analysis. 
           Therefore,  
           this report does not change as the module is filtered for traits.</i>
         <div id="enrich-info"><br><b>Please select a Module</b></div>
       </div>   

       <div id="tripal-module-explorer-genetics" class="tripal-module-explorer-item">
         <b>Overlap with Genetic Features</b>
         <div id="genetics-loading" class="tripal-network-loading-box" style="display: none"><p>Loading...</br><img src="<?php print $base_url . "/" . drupal_get_path('module', 'tripal_network') . '/theme/images/ajax-loader.gif' ?>"></p></div>
         <br><i>Genes in this module are 'contained by' (e.g. QTL) or 'adjacent to' (e.g. SNP) the 
         following genetic features.  This report will change automatically when the trait filters are changed.</i>
         <p><div id="genetics-info"><b>Please select a Module</b></div></p>
       </div>   

       <div id="tripal-module-explorer-trait" class="tripal-module-explorer-item">
         <b>Filter By Trait</b>
         <div id="trait-loading" class="tripal-network-loading-box" style="display: none"><p>Loading...</br><img src="<?php print $base_url . "/" . drupal_get_path('module', 'tripal_network') . '/theme/images/ajax-loader.gif' ?>"></p></div>
         <br><i>The following form allows you to adjust this report by 
         selected values in the form below.  <font color=red>Please note, that
         updates to the reports may be slow for large modules.</font></i>
         <?php print $trait_select_form ?></br>         
       </div>    

       <div id="tripal-module-explorer-markers" class="tripal-module-explorer-item">
         <b>Potential Markers</b>
         <div id="markers-loading" class="tripal-network-loading-box" style="display: none"><p>Loading...</br><img src="<?php print $base_url . "/" . drupal_get_path('module', 'tripal_network') . '/theme/images/ajax-loader.gif' ?>"></p></div>
         <br><i>Use this form to generate potential biomarkers for the nodes in this module. If 
         you have entered a trait filter then only those nodes that overlap the filter will be
         used for finding nearby biomarkers.  If no filter is provided then all nodes will be used.  </i>
         <?php print $flanking_markers_form ?><br>
         <div id="marker-info"></div>
       </div>

    </div>        
  </div> <?php 
}
?>
