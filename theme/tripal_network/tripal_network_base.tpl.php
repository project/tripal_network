<?php

// get the network and it's description property
$network  = $variables['node']->network;
$description = tripal_network_get_property($network->network_id, 'network_description');

// get the network stats
$properties = array(
  'module_count' => 0,
  'node_count' => 0,
  'edge_count' => 0,
  'average_degree' => 0,
);
tripal_network_network_topological_properties($network->network_id, $properties);
$mod_count = $properties['module_count'];
$node_count = $properties['node_count'];
$edge_count = $properties['edge_count'];
$avg_degree = $properties['average_degree'];
        
?>
<div id="tripal_network-base-box" class="tripal_network-info-box tripal-info-box">
  <div class="tripal_network-info-box-title tripal-info-box-title">Network Details</div>
  <div class="tripal_network-info-box-desc tripal-info-box-desc"></div>

  <table id="tripal_network-base-table" class="tripal_network-table tripal-table tripal-table-vert">
    <tr class="tripal_network-table-even-row tripal-table-even-row">
      <th nowrap>Network Name</th>
      <td><?php print $network->name; ?></td>
    </tr>
    <tr class="tripal_network-table-odd-row tripal-table-odd-row">
      <th>Internal ID</th>
      <td><?php print $network->network_id; ?></td>
    </tr>
    <tr class="tripal_network-table-even-row tripal-table-even-row">
      <th>Organism</th>
      <td>
        <?php if ($network->organism_id->nid) { 
         print "<a href=\"".url("node/".$network->organism_id->nid)."\">".$network->organism_id->genus ." " . $network->organism_id->species ." (" .$network->organism_id->common_name .")</a>";         
        } else { 
          print $network->organism_id->genus ." " . $network->organism_id->species ." (" .$network->organism_id->common_name .")";
        } ?>
      </td>
    </tr>      
    <tr class="tripal_network-table-odd-row tripal-table-odd-row">
      <th>Network Type</th>
      <td><?php print $network->type_id->name ?>
      </td>
    </tr>                                              
    <tr class="tripal_network-table-even-row tripal-table-even-row"> 
       <th>Network Stats</th>
       <td>
			   <table>
           <tr> 
             <td>Number of Modules</td> 
             <td align="right"><?php print number_format($mod_count) ?></td>
           </tr>
			     <tr>
			       <td>Number of Nodes</td> 
			       <td align="right"><?php print number_format($node_count) ?></td>
			     </tr>
			     <tr> 
			       <td>Number of Edges</td> 
			       <td align="right"><?php print number_format($edge_count) ?></td>
			     </tr>
           <tr> 
             <td>Average Degree</td> 
             <td align="right"><?php printf("%.2f", $avg_degree); ?></td>
           </tr>
			   </table>
			 </td>
		</tr><?php
    if ($description->value) { ?>
      <tr class="tripal_network-table-odd-row tripal-table-odd-row">
        <th>Description</th>
        <td><?php print $description->value ?>
        </td>
      </tr> <?php
    } ?>
   </table>
</div>
