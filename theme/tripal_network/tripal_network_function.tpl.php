<?php

// get the network and it's description property
$network  = $variables['node']->network;
        
?>
<div id="tripal_network-function-box" class="tripal_network-info-box tripal-info-box">
  <div class="tripal_network-info-box-title tripal-info-box-title">Network Details</div>
  <div class="tripal_network-info-box-desc tripal-info-box-desc"></div>

  <table id="tripal_network-function-table" class="tripal_network-table tripal-table tripal-table-vert">
  </table>
</div>
