<?php

$network = $variables['node']->network;

$all_relationships = $network->all_relationships;
$object_rels = $all_relationships['object'];
$subject_rels = $all_relationships['subject'];

if (count($object_rels) > 0 or count($subject_rels) > 0) {
?>
  <div id="tripal_network-relationships-box" class="tripal_network-info-box tripal-info-box">
    <div class="tripal_network-info-box-title tripal-info-box-title">Relationships</div>
    <div class="tripal_network-info-box-desc tripal-info-box-desc"></div> <?php
    
      // first add in the subject relationships.  
      foreach ($subject_rels as $rel_type => $rels){
         foreach ($rels as $parent_type => $parents){?>
           <p>This <?php print $network->type_id->name;?> network is <?php print $rel_type ?> the following <b><?php print $parent_type ?></b> network(s):
           <table id="tripal_network-relationships_as_object-table" class="tripal_network-table tripal-table tripal-table-horz">
             <tr>
               <th>Network Name</th>
               <th>Type</th>
             </tr> <?php
             foreach ($parents as $parent){ ?>
               <tr>
                 <td><?php 
                    if ($parent->record->nid) {
                      print "<a href=\"" . url("node/" . $parent->record->nid) . "\" target=\"_blank\">" . $parent->record->object_id->name . "</a>";
                    }
                    else {
                      print $parent->record->object_id->name;
                    } ?>
                 </td>
                 <td><?php print $parent->record->object_id->type_id->name ?></td>                 
               </tr> <?php
             } ?>
             </table>
             </p><br><?php
         }
      }
      
      // second add in the object relationships.  
      foreach ($object_rels as $rel_type => $rels){
         foreach ($rels as $child_type => $children){?>
           <p>The following <b><?php print $child_type ?></b> network(s) are <?php print $rel_type ?> this <?php print $network->type_id->name;?>:
           <table id="tripal_network-relationships_as_object-table" class="tripal_network-table tripal-table tripal-table-horz">
             <tr>
               <th>Network Name</th>
               <th>Type</th>
             </tr> <?php
             foreach ($children as $child){ ?>
               <tr>
                 <td><?php 
                    if ($child->record->nid) {
                      print "<a href=\"" . url("node/" . $child->record->nid) . "\" target=\"_blank\">" . $child->record->subject_id->name . "</a>";
                    }
                    else {
                      print $child->record->subject_id->name;
                    } ?>
                 </td>
                 <td><?php print $child->record->subject_id->type_id->name ?></td>                 
               </tr> <?php
             } ?>
             </table>
             </p><br><?php
         }
      }
      
      
    ?>
  </div> <?php
}
