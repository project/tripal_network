<?php

// get the network and it's description property
$network  = $variables['node']->network;

$module_select_form = $variables['module_select_form'];

?>
<script type="text/javascript">

</script>

<div id="tripal_network-viewer-box" class="tripal_network-info-box tripal-info-box">
  <div class="tripal_network-info-box-title tripal-info-box-title">Module Viewer</div>
  <div class="tripal_network-info-box-desc tripal-info-box-desc"></div>

  <?php print $module_select_form ?> 
  <div id="cytoscapeweb"></div>
</div>
