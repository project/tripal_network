<?php
$organism = $variables['node']->organism;

// expand the organism object to include the libraries from the network_organism
// table in chado.
$options = array('return_array' => 1, 'order_by' => array('name' => 'ASC'));
$organism = tripal_core_expand_chado_vars($organism, 'table', 'network', $options);
$networks = $organism->network;


// don't show this page if there are no networks
if (count($networks) > 0) { ?>
  <div id="tripal_organism-networks-box" class="tripal_organism-info-box tripal-info-box">
    <div class="tripal_organism-info-box-title tripal-info-box-title">Networks</div>
    <div class="tripal_organism-info-box-desc tripal-info-box-desc">The following network are available for this species. Click on a network for further details and exploration.</div>
    <table id="tripal_organism-networks-table" class="tripal_organism-table tripal-table tripal-table-horz">
      <tr>
        <th>Network Name</th>
        <th>Network Type</th>
        <th>Module Count</th>
        <th>Edge Count</th>
        <th>Node Count</th>
        <th>Average Degree &lt;<i>k</i>&gt;</th>
      </tr> <?php
      $i = 0; 
      foreach ($networks as $network) {
        $class = 'tripal-table-odd-row';
        $term = $network->type_id;        
        $term = tripal_core_expand_chado_vars($term, 'field', 'cvterm.definition', $options);
          
        // get the network properties
        $properties = array();
        $properties['module_count'] = 0;
        $properties['node_count'] = 0;
        $properties['edge_count'] = 0;
        $properties['average_degree'] = 0;
        tripal_network_network_topological_properties($network->network_id, $properties);
        $mod_count = $properties['module_count'];
        $node_count = $properties['node_count'];
        $edge_count = $properties['edge_count'];
        $avg_degree = $properties['average_degree'];
             
        // now build the table row
        if ($i % 2 == 0 ) {
           $class = 'tripal-table-even-row';
        } ?>
        <tr class="<?php print $class ?>">
          <td><?php 
            if ($network->nid) {
               print "<a href=\"". url("node/".$network->nid) . "\">" . $network->name . "</a>";
            } else {
               print $network->name;
            } ?>
          </td>
          <td align=right><?php print $term->name ?></td>
          <td align=right><?php print number_format($mod_count) ?></td>
          <td align=right><?php print number_format($edge_count) ?></td>
          <td align=right><?php print number_format($node_count) ?></td>
          <td align=right><?php printf("%.2f", $avg_degree);?></td>
        </tr> <?php
        $i++;  
      } ?>
    </table> 
  </div><?php 
} 

